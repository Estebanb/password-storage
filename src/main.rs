use std::fs::OpenOptions;
use std::error::Error;
use serde::{Serialize, Deserialize};


extern crate bincode;

#[derive(Serialize, Deserialize, Debug, PartialEq)]
struct Password{
    username: String,
    password: String,
    url: String
}

impl Password{
    pub fn new(username: String, password: String, url: String) -> Password {
        Password{
            username,
            password,
            url
        }
    }
    // Without the Box<dyn Error> is not possible to return different kind of trait errors with the ? operator.
    pub fn save_password_to_file(&self) -> Result<(), Box<dyn Error>> {
        let mut writer = OpenOptions::new().write(true).create(true).open("password_manager.txt")?;
        bincode::serialize_into(&mut writer, &self)?;
        Ok(())
    }
    pub fn read_password_from_file() -> Result<Password, Box<dyn Error>> {
        let mut reader = OpenOptions::new().read(true).open("password_manager.txt")?;
        let decoded: Password = bincode::deserialize_from(&mut reader)?;
        Ok(decoded)
    }
}


fn main() {
    println!("Hello, world!");
    let pass = Password::new(String::from("Esteban"),String::from("passworddd"),String::from("https://holu.com"));
    // Define how to handle errors to remove unwrap().
    pass.save_password_to_file().unwrap();
    // let readed_pass = Password::read_password_from_file().unwrap();
}

#[test]
fn test_create_new_password(){
    let pass = Password::new(String::from("pepitox"),
                             String::from("123456"),
                             String::from("https://pepitox.com/private/guakale"));
    assert_eq!(pass.username, String::from("pepitox"));
    assert_eq!(pass.password, String::from("123456"));
    assert_eq!(pass.url, String::from("https://pepitox.com/private/guakale"));
}

#[test]
fn test_save_and_password_to_file(){
    let write = Password::new(String::from("pepitox"),
                              String::from("123456"),
                              String::from("https://pepitox.com/private/guakale"));
    write.save_password_to_file().unwrap();
    let read = Password::read_password_from_file().unwrap();
    assert_eq!(write, read);
}
